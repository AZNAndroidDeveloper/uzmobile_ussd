package uz.app.uzmobile_gsm.advertisementAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import uz.app.uzmobile_gsm.R
import uz.app.uzmobile_gsm.databinding.ViewHolderAdvertisementBinding as AdvertisementViewBinding
import uz.app.uzmobile_gsm.model.AdvertisementModel

class AdvertisementAdapter(
    private val itemClickListener: (AdvertisementModel)->Unit
):RecyclerView.Adapter<AdvertisementAdapter.AdvertisementViewHolder>() {
   private val elements: MutableList<AdvertisementModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdvertisementViewHolder {
        return AdvertisementViewHolder(AdvertisementViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: AdvertisementViewHolder, position: Int) {
        holder.bind(elements[position])
    }

    fun setData(newElements: List<AdvertisementModel>) {
            elements.apply { clear(); addAll(newElements) }
        notifyDataSetChanged()
    }

    inner class AdvertisementViewHolder(val binding:AdvertisementViewBinding):RecyclerView.ViewHolder(binding.root){

        fun bind(element: AdvertisementModel){
            with(binding) {
                tvRate.text = element.rate
                textTime.text = element.time
                textMb.text = element.mb
                textMessage.text = element.message
                monthly.text = element.month
                itemParent.setOnClickListener {
                    itemClickListener.invoke(element)
                }
            }
        }
    }
}