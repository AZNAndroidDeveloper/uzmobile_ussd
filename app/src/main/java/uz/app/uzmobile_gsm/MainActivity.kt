package uz.app.uzmobile_gsm

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import uz.app.uzmobile_gsm.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val toggle: ActionBarDrawerToggle = ActionBarDrawerToggle(this, binding.drawerLayout, binding.toolbar,
            R.string.open, R.string.close
        )
        with(binding){
            drawerLayout.addDrawerListener(toggle)
            navView.setNavigationItemSelectedListener (this@MainActivity)
        }
toggle.syncState()
//        val navHostFragment= supportFragmentManager.findFragmentById(R.id.fragment_container_view_tag) as NavHostFragment
//
//        binding.toolbar.setupWithNavController( navController = navHostFragment.navController)
//
//        navHostFragment.navController.addOnDestinationChangedListener{controller, destination, arguments ->
//            binding.toolbar.visibility = View.INVISIBLE
//        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.home->{ Toast.makeText(applicationContext, "HomePage", Toast.LENGTH_SHORT).show()}
            R.id.setting->{ Toast.makeText(applicationContext, "Setting", Toast.LENGTH_SHORT).show()}
            R.id.log_out->{ Toast.makeText(applicationContext, "Logout", Toast.LENGTH_SHORT).show()}

        }
        with(binding){
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        return true

    }


}