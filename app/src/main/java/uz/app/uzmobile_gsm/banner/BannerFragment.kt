package uz.app.uzmobile_gsm.banner

import android.content.ClipData
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import android.widget.Toolbar
import androidx.navigation.fragment.navArgs
import uz.app.uzmobile_gsm.MainActivity
import uz.app.uzmobile_gsm.R
import uz.app.uzmobile_gsm.databinding.FragmentBannerBinding

class BannerFragment : Fragment(R.layout.fragment_banner) {
    private lateinit var  binding:FragmentBannerBinding
    private var mainActivity = MainActivity()
   // private  var toolbar:Toolbar? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentBannerBinding.bind(view)

   with(binding){
       tvRate.text = fragmentArgs.adversitement.rate
       textTime.text = fragmentArgs.adversitement.time
       textMb.text  = fragmentArgs.adversitement.mb
       textMessage.text = fragmentArgs.adversitement.message
       monthly.text = fragmentArgs.adversitement.month
   }

        Toast.makeText(requireContext(), fragmentArgs.adversitement.message, Toast.LENGTH_SHORT).show()
    }

    private val fragmentArgs by lazy {
        navArgs<BannerFragmentArgs>().value
    }

//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        mainActivity = context as MainActivity
//
//    }
//    private fun setUpToolbar(){
//        toolbar?.title = "Salom"
//
//    }
}