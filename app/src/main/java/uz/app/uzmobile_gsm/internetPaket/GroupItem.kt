package uz.app.uzmobile_gsm.internetPaket

import uz.app.uzmobile_gsm.monthFragment.model.MonthlyModel

class GroupItem(val header:List<Header>,val item:Item) {
    class Header(val textHeader:List<MonthlyModel>)
    class Item(val item:String){
        constructor():this("")
    }
}