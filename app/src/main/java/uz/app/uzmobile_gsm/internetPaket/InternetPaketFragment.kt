package uz.app.uzmobile_gsm.internetPaket

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import uz.app.uzmobile_gsm.R
import uz.app.uzmobile_gsm.databinding.FragmentInternetPaketBinding
import uz.app.uzmobile_gsm.dayFragment.DayFragment
import uz.app.uzmobile_gsm.fragmentAdapter.AdapterFragment
import uz.app.uzmobile_gsm.monthFragment.MonthlyFragment
import uz.app.uzmobile_gsm.weekFragment.WeekFragment
import uz.app.uzmobile_gsm.yearlyFragment.YearlyFragment


class InternetPaketFragment : Fragment(R.layout.fragment_internet_paket) {
   private lateinit var binding: FragmentInternetPaketBinding
   override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
      super.onViewCreated(view, savedInstanceState)
      binding = FragmentInternetPaketBinding.bind(view)
      val adapterFragment = AdapterFragment(childFragmentManager)

      adapterFragment.setFragment("Kunlik",DayFragment())
      adapterFragment.setFragment("Haftalik",WeekFragment())
      adapterFragment.setFragment("Oylik",MonthlyFragment())
      adapterFragment.setFragment("Yillik",YearlyFragment())
      with(binding){
         viewPager.adapter = adapterFragment
         tabLayout.setupWithViewPager(binding.viewPager)
      }
      setMarginOnTabItems()

   }
   fun setMarginOnTabItems() {
      for (i in 0 until binding.tabLayout.tabCount) {
         val tabItem = (binding.tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
         val paramsMargin = tabItem.layoutParams as ViewGroup.MarginLayoutParams
         paramsMargin.setMargins(16, 4, 16, 4)
         tabItem.setPadding(10,4,10,4)

      }
   }

   }