package uz.app.uzmobile_gsm.homePage

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.google.android.material.navigation.NavigationView
import uz.app.uzmobile_gsm.R
import uz.app.uzmobile_gsm.advertisementAdapter.AdvertisementAdapter
import uz.app.uzmobile_gsm.databinding.FragmentHomePageBinding
import uz.app.uzmobile_gsm.internetPaket.InternetPaketFragment
import uz.app.uzmobile_gsm.model.AdvertisementModel

class HomePageFragment : Fragment(R.layout.fragment_home_page){
    private lateinit var binding: FragmentHomePageBinding
    private val advertisementAdapter = AdvertisementAdapter {
        findNavController().navigate(HomePageFragmentDirections.actionHomePageFragmentToBannerFragment(it))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentHomePageBinding.bind(view)

        with(binding) {
            imageSliders.setOnClickListener {
                findNavController().navigate(HomePageFragmentDirections.actionNavigationGlobalHomePageFragmentToInternetPaketFragment())
            }
            recyclerView.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                adapter = advertisementAdapter

                val snapHelper = PagerSnapHelper()
                snapHelper.attachToRecyclerView(this)

                indicator.attachToRecyclerView(recyclerView)
            }
        }
        getTarifs().let { advertisementAdapter.setData(it) }


    }

    private fun getTarifs()  = arrayListOf(
            AdvertisementModel("Просто 10", "10 minut", "10 mb", "10 sms", "10000 сум в месяц"),
            AdvertisementModel("Мактаб", "200 minut", "5000 MB", "500 SMS", "29900 сум в месяц"),
            AdvertisementModel("Онлиме", "1000 minut", "10000 mb", "1000 sms", "49900 сум в месяц"),
            AdvertisementModel("Ажойиб", "cheksiz", "1000 mb", "1000 sms", "39900 сум в месяц"),
            AdvertisementModel("Оптимал", "350 minut", "350 mb", "350 sms", "20900 сум в месяц"),
            AdvertisementModel("Роял", "cheksiz", "cheksiz", "5000 sms", "149900 сум в месяц")
        )


    }

