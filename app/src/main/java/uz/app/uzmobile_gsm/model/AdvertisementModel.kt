package uz.app.uzmobile_gsm.model

import java.io.Serializable

data class AdvertisementModel (
    val  rate:String,
    val time:String,
    val mb:String,
    val message:String,
    val month:String
):Serializable