package uz.app.uzmobile_gsm.monthFragment.recycleAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import uz.app.uzmobile_gsm.R
import uz.app.uzmobile_gsm.databinding.FragmentMonthlyBinding
import uz.app.uzmobile_gsm.internetPaket.GroupItem

var isExpanded = false

class MonthlyRecycleAdapter(private val groupItem: GroupItem) :
    RecyclerView.Adapter<MonthlyRecycleAdapter.MonthlyAdapter>() {

    companion object {
        const val VIEW_HEADER = 0
        const val VIEW_SUB = 1
    }


    sealed class MonthlyAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var isHave = true

        class HeaderViewHolder(itemView: View) : MonthlyAdapter(itemView) {
            val headerTextView = itemView.findViewById<TextView>(R.id.tv_desc)
            val textTitle = itemView.findViewById<TextView>(R.id.text_title)
            val tvCounter = itemView.findViewById<TextView>(R.id.tv_counter)
            val imageDown = itemView.findViewById<ImageView>(R.id.image_down)
            val view = itemView.findViewById<View>(R.id.view_header)


            fun onBind(header: GroupItem.Header, onClickListener: View.OnClickListener) {
                headerTextView.text = header.textHeader.toString()

                if (!isExpanded) {
                    imageDown.setImageResource(R.drawable.ic_arrow_down_navigate)

                } else {
                    imageDown.setImageResource(R.drawable.ic_navigate_up_arrow)
                }
                imageDown.setOnClickListener {

                    onClickListener.onClick(it)


                }

            }
        }

        class SubItemViewHolder(itemView: View) : MonthlyAdapter(itemView) {
            val headerTextView = itemView.findViewById<TextView>(R.id.tv_desc2)
            val btnTetx = itemView.findViewById<Button>(R.id.btn_ulanish)
            fun onBind(item: GroupItem.Item) {
                headerTextView.text = item.item.toString()
            }

        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MonthlyAdapter {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_HEADER -> {
                MonthlyAdapter.HeaderViewHolder(
                    layoutInflater.inflate(R.layout.header_item, parent, false)
                )
            }
            else -> {
                MonthlyAdapter.SubItemViewHolder(
                    layoutInflater.inflate(
                        R.layout.sub_item,
                        parent,
                        false
                    )
                )
            }

        }

    }

    override fun getItemCount(): Int {
        return if (!isExpanded) {
            1
        } else
            groupItem.header.size + 1
    }

    override fun onBindViewHolder(holder: MonthlyAdapter, position: Int) {
        when (holder) {
            is MonthlyAdapter.HeaderViewHolder -> holder.onBind(
                groupItem.header[position],
                onHeaderClicked()
            )
            is MonthlyAdapter.SubItemViewHolder -> holder.onBind(groupItem.item)
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            VIEW_HEADER
        } else
            VIEW_SUB
    }

    fun onHeaderClicked() = View.OnClickListener {
        isExpanded = !isExpanded
        if (isExpanded) {
            notifyItemRangeInserted(1, groupItem.header.size)
            notifyItemChanged(0)
        } else {
            notifyItemRangeRemoved(1, groupItem.header.size)
            notifyItemChanged(1)
        }
    }

}