package uz.app.uzmobile_gsm.monthFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import uz.app.uzmobile_gsm.R
import uz.app.uzmobile_gsm.databinding.FragmentMonthlyBinding
import uz.app.uzmobile_gsm.internetPaket.GroupItem
import uz.app.uzmobile_gsm.monthFragment.model.MonthlyModel
import uz.app.uzmobile_gsm.monthFragment.recycleAdapter.MonthlyRecycleAdapter

class MonthlyFragment :Fragment(R.layout.fragment_monthly){
    private lateinit var binding:FragmentMonthlyBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMonthlyBinding.bind(view)
        val adapterList  = arrayListOf<MonthlyRecycleAdapter>()
        val groupList =  createItem(10)
        for (item in groupList){
            val adapter  =MonthlyRecycleAdapter(item)
            adapterList.add(adapter)
        }
        val concatConfig  = ConcatAdapter.Config.Builder().setIsolateViewTypes(false).build()
        val adapter = ConcatAdapter(concatConfig,adapterList)
        binding.recycleView.layoutManager = LinearLayoutManager(requireActivity().applicationContext,
            LinearLayoutManager.VERTICAL,false)
        binding.recycleView.adapter = adapter
    }
    fun createItem(numberOfHeader:Int):List<GroupItem>{
        val groupList = arrayListOf<GroupItem>()
        for (i in 0 until numberOfHeader){
            val header = arrayListOf<GroupItem.Header>()
            header.add(GroupItem.Header(setModelList()))
            var item  = GroupItem.Item()
            for (i in 0 until 1){
                 item = GroupItem.Item(resources.getString(R.string.content_two))
            }
val groupItem = GroupItem(header,item )
            groupList.add(groupItem)
        }
        return groupList
    }


    fun setModelList():List<MonthlyModel> = arrayListOf(
        MonthlyModel(50,"50 MB",resources.getString(R.string.content)),
        MonthlyModel(100,"100 MB",resources.getString(R.string.content)),
        MonthlyModel(150,"150 MB",resources.getString(R.string.content)),
        MonthlyModel(500,"500 MB",resources.getString(R.string.content)),
        MonthlyModel(1000,"1000 MB",resources.getString(R.string.content)),
        MonthlyModel(2,"2 GB",resources.getString(R.string.content)),
        MonthlyModel(2,"2 GB",resources.getString(R.string.content)),
        MonthlyModel(2,"2 GB",resources.getString(R.string.content))
    )

}