package uz.app.uzmobile_gsm.fragmentAdapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class AdapterFragment(fragmentManager: FragmentManager):FragmentPagerAdapter(fragmentManager){
    private val fragment: MutableList<Fragment> = arrayListOf()
    private  var element:MutableList<String> = arrayListOf()
    override fun getItem(position: Int): Fragment  = fragment[position]

    override fun getCount(): Int  = element.size

     fun setFragment(element:String, fragment: Fragment){
     this.fragment.add(fragment)
        this.element.add(element)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return element[position]
    }
}